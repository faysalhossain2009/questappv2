﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Windows.Navigation;

namespace HangmanPhoneApp
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            // Save the chosen settings
            if (Easy.IsChecked.Value)
                AppSettings.level.Value = AppSettings.GameLevel.Easy.ToString();
            else if (Medium.IsChecked.Value)
                AppSettings.level.Value = AppSettings.GameLevel.Medium.ToString();
            else if (Hard.IsChecked.Value)
                AppSettings.level.Value = AppSettings.GameLevel.Hard.ToString();
            else
                AppSettings.level.Value = AppSettings.GameLevel.Easy.ToString();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            // Load the saved settings
            if (AppSettings.level.Value == AppSettings.GameLevel.Easy.ToString())
                Easy.IsChecked = true;
            else if (AppSettings.level.Value == AppSettings.GameLevel.Medium.ToString())
                Medium.IsChecked = true;
            else if (AppSettings.level.Value == AppSettings.GameLevel.Hard.ToString())
                Hard.IsChecked = true;
            else
                Easy.IsChecked = true;

            ShowWord.Content = AppSettings.showWord.Value == true ? "Yes" : "No";
            
        }

        private void ShowWord_Checked(object sender, RoutedEventArgs e)
        {
            ShowWord.Content = "Yes";
            AppSettings.showWord.Value = true;
        }

        private void ShowWord_Unchecked(object sender, RoutedEventArgs e)
        {
            ShowWord.Content = "No";
            AppSettings.showWord.Value = false;
        }
    }
}
