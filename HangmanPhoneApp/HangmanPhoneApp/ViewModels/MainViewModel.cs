﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using HangmanPhoneApp.Resources;
using quest;
using System.Diagnostics;

namespace HangmanPhoneApp.ViewModels
{
    public class MainViewModel:INotifyPropertyChanged
    {
        public MainViewModel() {
            this.Items = new ObservableCollection<ItemViewModel>();
        }

        public ObservableCollection<ItemViewModel> Items
        {
            get;
            private set;
        }

        private string _sampleProperty = "Sample Runtime Property Value";

        public string SampleProperty
        {
            get {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
            
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        public void LoadData()
        {
            

            
            foreach (string s in App.roomIds)
            {
                Debug.WriteLine(s);
                this.Items.Add(new ItemViewModel()
                {
                    ID = s,
                    RoomName = "Myroom "+s
                });
            }

            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
