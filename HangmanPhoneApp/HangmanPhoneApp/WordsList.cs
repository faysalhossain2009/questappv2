﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;

namespace HangmanPhoneApp
{
    public class WordsList
    {
        List<string> _wordList = new List<string>();
        string _currentWord = "";

        public WordsList() 
        {
            ReadWordFromDictionary();
        }
    

        /// <summary>
        /// Read words from dictionary
        /// </summary>
        public void ReadWordFromDictionary()
        {
            string fileName = "HangmanPhoneApp.Dictionary.corncob_lowercase.txt";
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            string[] resources = assembly .GetManifestResourceNames();

            using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(fileName), System.Text.Encoding.UTF8))
            {
                while (!reader.EndOfStream)
                {
                    string s = reader.ReadLine().Trim();
                    _wordList.Add(s);
                }
            }

        }

        public int MaximumWordLength
        {
            get; set;
        }

        /// <summary>
        /// Returns a word of set "Length"
        /// </summary>        
        public string Word
        {
            get
            {
                if (_currentWord == "")
                    _currentWord = GetWordOfLength(MaximumWordLength);

                return _currentWord;
            }

            set { _currentWord = value; }
        }

        /// <summary>
        /// Returns a word of given length. If length is 0 or less than 0 then word of any length is returned
        /// </summary>
        /// <param name="length">Lngth of word. To return word of any length, pass 0 or any number less than 0</param>
        /// <returns></returns>
        private string GetWordOfLength(int length)
        {
            int minWordLength = (length <= 3) ? 3 : (length <= 6) ? 4 : 6;
            bool validWord = false;
            Random random = new Random();
            do
            {
                int index = random.Next(_wordList.Count);
                string s = _wordList[index];
                if ((s.Length <= length && s.Length >= minWordLength )|| length <= 0) return _wordList[index];
                validWord = false;
            }
            while (!validWord);

            // Default is to return en empty string
            return "";
        }
    }
}
