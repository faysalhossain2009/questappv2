﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
  
        /*
        public class Option
        {
            public int ID { get; set; }
            public string OptionBody { get; set; }
            public Question Question { get; set; }
            public string ImageURL { get; set; }
        }
        */

        [Table]
        public class Option : INotifyPropertyChanged, INotifyPropertyChanging
        {
            // Define ID: private field, public property and database column.
            private int _id;

            [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
            public int OptionID
            {
                get
                {
                    return _id;
                }
                set
                {
                    if (_id != value)
                    {
                        NotifyPropertyChanging("OptionID");
                        _id = value;
                        NotifyPropertyChanged("OptionID");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private string _optionBody;

            [Column]
            public string OptionBody
            {
                get
                {
                    return _optionBody;
                }
                set
                {
                    if (_optionBody != value)
                    {
                        NotifyPropertyChanging("OptionBody");
                        _optionBody = value;
                        NotifyPropertyChanged("OptionBody");
                    }
                }
            }

            // Define _imageURL: private field, public property and database column.
            private string _imageURL;

            [Column]
            public string ImageURL
            {
                get
                {
                    return _imageURL;
                }
                set
                {
                    if (_imageURL != value)
                    {
                        NotifyPropertyChanging("ImageURL");
                        _imageURL = value;
                        NotifyPropertyChanged("ImageURL");
                    }
                }
            }

            /*
            // Define Question: private field, public property and database column.
            private Question _question;

            [Column]
            public Question Question
            {
                get
                {
                    return _question;
                }
                set
                {
                    if (_question != value)
                    {
                        NotifyPropertyChanging("Question");
                        _question = value;
                        NotifyPropertyChanged("Question");
                    }
                }
            }
            */

          //  [Column(IsPrimaryKey = true)]
          //  public int OptionID;
            [Column]
            internal int _QuestionID;

            private EntityRef<Question> _Question;

            [Association(Storage = "_Question", ThisKey = "_QuestionID", OtherKey = "QuestionID", IsForeignKey = true)]
            public Question Question
            {
                get { return this._Question.Entity; }
                set { 
                    this._Question.Entity = value;
                    if (value != null)
                    {
                        _QuestionID = value.QuestionID;
                    }
                }
            }





            // public Question Question { get; set; }

            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            // Used to notify the page that a data context property changed
            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            #endregion

            #region INotifyPropertyChanging Members

            public event PropertyChangingEventHandler PropertyChanging;

            // Used to notify the data context that a data context property is about to change
            private void NotifyPropertyChanging(string propertyName)
            {
                if (PropertyChanging != null)
                {
                    PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
                }
            }

            #endregion
        }
    
}
