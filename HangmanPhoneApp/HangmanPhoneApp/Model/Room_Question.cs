﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
     [Table]
    public class Room_Question : INotifyPropertyChanged, INotifyPropertyChanging
    {
      
        // Define ID: private field, public property and database column.
        private int _id;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int RoomQuestionID
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    NotifyPropertyChanging("RoomQuestionID");
                    _id = value;
                    NotifyPropertyChanged("RoomQuestionID");
                }
            }
        }

        [Column]
        internal int _roomID;

        private EntityRef<Room> _Room;

        [Association(Storage = "_Room", ThisKey = "_roomID", OtherKey = "RoomID", IsForeignKey = true)]
        public Room UserProfile
        {
            get { return this._Room.Entity; }
            set
            {
                this._Room.Entity = value;
                if (value != null)
                {
                    _roomID = value.RoomID;
                }
            }
        }

        [Column]
        internal int _questionID;

        private EntityRef<Question> _Question;

        [Association(Storage = "_Question", ThisKey = "_questionID", OtherKey = "QuestionID", IsForeignKey = true)]
        public Question Acheivement
        {
            get { return this._Question.Entity; }
            set
            {
                this._Question.Entity = value;
                if (value != null)
                {
                    _questionID = value.QuestionID;
                }
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion

    }
}
