﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
     [Table]
   public class UserProfile_Achievement : INotifyPropertyChanged, INotifyPropertyChanging
    {
      /* [Table(Name = "Customers")]
        public partial class Customer
        {
            [Column(IsPrimaryKey = true)]
            public string CustomerID;
            // ... 
            private EntitySet<Order> _Orders;
            [Association(Storage = "_Orders", OtherKey = "CustomerID")]
            public EntitySet<Order> Orders
            {
                get { return this._Orders; }
                set { this._Orders.Assign(value); }
            }
        }


        [Table(Name = "Orders")]
        public class Order
        {
            [Column(IsPrimaryKey = true)]
            public int OrderID;
            [Column]
            public string CustomerID;
            private EntityRef<Customer> _Customer;
            [Association(Storage = "_Customer", ThisKey = "CustomerID")]
            public Customer Customer
            {
                get { return this._Customer.Entity; }
                set { this._Customer.Entity = value; }
            }
        }
       */

        // Define ID: private field, public property and database column.
        private int _id;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int UserProfileAchievementID
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    NotifyPropertyChanging("UserProfileAchievementID");
                    _id = value;
                    NotifyPropertyChanged("UserProfileAchievementID");
                }
            }
        }

        [Column]
        internal int _userID;

        private EntityRef<UserProfile> _UserProfile;

        [Association(Storage = "_UserProfile", ThisKey = "_userID", OtherKey = "UserProfileID", IsForeignKey = true)]
        public UserProfile UserProfile
        {
            get { return this._UserProfile.Entity; }
            set
            {
                this._UserProfile.Entity = value;
                if (value != null)
                {
                    _userID = value.UserProfileID;
                }
            }
        }

        [Column]
        internal int _achievementID;

        private EntityRef<Achievement> _Achievement;

        [Association(Storage = "_Achievement", ThisKey = "_achievementID", OtherKey = "AchievementID", IsForeignKey = true)]
        public Achievement Acheivement
        {
            get { return this._Achievement.Entity; }
            set
            {
                this._Achievement.Entity = value;
                if (value != null)
                {
                    _achievementID = value.AchievementID;
                }
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion

    }
}
