﻿
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
    public class QuestDataContext : DataContext
    {
        
            // Pass the connection string to the base class.
        public QuestDataContext(string connectionString)
                : base(connectionString)
            { }

            // Specify a table for the to-do items.
            //public Table<User> Users;
            public Table<UserProfile> UserProfiles;
            public Table<Achievement> Achievements;
            public Table<Question> Questions;
            public Table<Option> Options;
            public Table<Category> Categories;
            public Table<Room> Rooms;
            public Table<Score> Scores;
           
            public Table<UserProfile_Achievement> UserProfile_Achievement;


           

        

    }
}
