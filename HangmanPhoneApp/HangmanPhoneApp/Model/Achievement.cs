﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
   
  /*
        public int ID { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
        public string Condition { get; set; }
        public List<UserProfile> Users { get; set; }   (many to many)
*/

        [Table]
        public class Achievement : INotifyPropertyChanged, INotifyPropertyChanging
        {
            // Define ID: private field, public property and database column.
            private int _id;

            [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
            public int AchievementID
            {
                get
                {
                    return _id;
                }
                set
                {
                    if (_id != value)
                    {
                        NotifyPropertyChanging("AchievementID");
                        _id = value;
                        NotifyPropertyChanged("AchievementID");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private string _name;

            [Column]
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (_name != value)
                    {
                        NotifyPropertyChanging("Name");
                        _name = value;
                        NotifyPropertyChanged("Name");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private string _imageURL;

            [Column]
            public string ImageURL
            {
                get
                {
                    return _imageURL;
                }
                set
                {
                    if (_imageURL != value)
                    {
                        NotifyPropertyChanging("ImageURL");
                        _imageURL = value;
                        NotifyPropertyChanged("ImageURL");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private string _condition;

            [Column]
            public string Condition
            {
                get
                {
                    return _condition;
                }
                set
                {
                    if (_condition != value)
                    {
                        NotifyPropertyChanging("Condition");
                        _condition = value;
                        NotifyPropertyChanged("Condition");
                    }
                }
            }

            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            // Used to notify the page that a data context property changed
            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            #endregion

            #region INotifyPropertyChanging Members

            public event PropertyChangingEventHandler PropertyChanging;

            // Used to notify the data context that a data context property is about to change
            private void NotifyPropertyChanging(string propertyName)
            {
                if (PropertyChanging != null)
                {
                    PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
                }
            }

            #endregion
        }

    
}
