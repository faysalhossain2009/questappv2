﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
    [Table]
    public class Room : INotifyPropertyChanged, INotifyPropertyChanging
    {
        /*
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public UserProfile Creator { get; set; }
        public Category Category { get; set; }
        public List<Question> Questiosn { get; set; } (many to many)
        public List<UserProfile> Users { get; set; } (many to many)

        */

        // Define ID: private field, public property and database column.
        private int _id;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int RoomID
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    NotifyPropertyChanging("RoomID");
                    _id = value;
                    NotifyPropertyChanged("RoomID");
                }
            }
        }


        // Define _questionBody: private field, public property and database column.
        private string _name;

        [Column]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)
                {
                    NotifyPropertyChanging("Name");
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        // Define _questionBody: private field, public property and database column.
        private DateTime _startTime;

        [Column]
        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }
            set
            {
                if (_startTime != value)
                {
                    NotifyPropertyChanging("StartTime");
                    _startTime = value;
                    NotifyPropertyChanged("StartTime");
                }
            }
        }

       

        // Define _questionBody: private field, public property and database column.

        [Column]
        internal int _categoryID;

        private EntityRef<Category> _Category;

        [Association(Storage = "_Category", ThisKey = "_categoryID", OtherKey = "CategoryID", IsForeignKey = true)]
        public Category Category
        {
            get { return this._Category.Entity; }
            set { 
                this._Category.Entity = value;
                if (value != null)
                {
                    _categoryID = value.CategoryID;
                }
            }
        }

        [Column]
        internal int _creatorID;

        private EntityRef<UserProfile> _UserProfile;

        [Association(Storage = "_UserProfile", ThisKey = "_creatorID", OtherKey = "UserProfileID", IsForeignKey = true)]
        public UserProfile Creator
        {
            get { return this._UserProfile.Entity; }
            set
            {
                this._UserProfile.Entity = value;
                if (value != null)
                {
                    _creatorID = value.UserProfileID;
                }
            }
        }

        // Define _imageURL: private field, public property and database column.
        //private UserProfile _creator;

        //[Column]
        //public UserProfile Creator
        //{
        //    get
        //    {
        //        return _creator;
        //    }
        //    set
        //    {
        //        if (_creator != value)
        //        {
        //            NotifyPropertyChanging("Creator");
        //            _creator = value;
        //            NotifyPropertyChanged("Creator");
        //        }
        //    }
        //}


        //many to many between room and question

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
