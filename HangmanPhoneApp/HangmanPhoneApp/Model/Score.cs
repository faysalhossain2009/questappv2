﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{

    /*
      public int ID { get; set; }
      public int Value { get; set; }
      public UserProfile User { get; set; }
      public Room Room { get; set; }
      public Category Category { get; set; }

     */
    [Table]
        public class Score : INotifyPropertyChanged, INotifyPropertyChanging
        {
          //   Define ID: private field, public property and database column.
            private int _scoreID;

            [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
            public int ScoreID
            {
                get
                {
                    return _scoreID;
                }
                set
                {
                    if (_scoreID != value)
                    {
                        NotifyPropertyChanging("ScoreID");
                        _scoreID = value;
                        NotifyPropertyChanged("ScoreID");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private int _value;

            [Column]
            public int Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    if (_value != value)
                    {
                        NotifyPropertyChanging("Value");
                        _value = value;
                        NotifyPropertyChanged("Value");
                    }
                }
            }

            [Column]
            internal int _categoryID;

            private EntityRef<Category> _Category;

            [Association(Storage = "_Category", ThisKey = "_categoryID", OtherKey = "CategoryID", IsForeignKey = true)]
            public Category Category
            {
                get { return this._Category.Entity; }
                set
                {
                    this._Category.Entity = value;
                    if (value != null)
                    {
                        _categoryID = value.CategoryID;
                    }
                }
            }


            [Column]
            internal int _userID;

            private EntityRef<UserProfile> _UserProfile;

            [Association(Storage = "_UserProfile", ThisKey = "_userID", OtherKey = "UserProfileID", IsForeignKey = true)]
            public UserProfile UserProfile
            {
                get { return this._UserProfile.Entity; }
                set
                {
                    this._UserProfile.Entity = value;
                    if (value != null)
                    {
                        _userID = value.UserProfileID;
                    }
                }
            }

            [Column]
            internal int _roomID;

            private EntityRef<Room> _RoomID;

            [Association(Storage = "_RoomID", ThisKey = "_roomID", OtherKey = "RoomID", IsForeignKey = true)]
            public Room Room
            {
                get { return this._RoomID.Entity; }
                set
                {
                    this._RoomID.Entity = value;
                    if (value != null)
                    {
                        _roomID = value.RoomID;
                    }
                }
            }

           //  Define _imageURL: private field, public property and database column.
            //private Category _category;

            //[Column]
            //public Category Category
            //{
            //    get
            //    {
            //        return _category;
            //    }
            //    set
            //    {
            //        if (_category != value)
            //        {
            //            NotifyPropertyChanging("Category");
            //            _category = value;
            //            NotifyPropertyChanged("Category");
            //        }
            //    }
            //}

           //  Define CorrectOption: private field, public property and database column.
            //private Room _room;

            //[Column]
            //public Room Room
            //{
            //    get
            //    {
            //        return _room;
            //    }
            //    set
            //    {
            //        if (_room != value)
            //        {
            //            NotifyPropertyChanging("Room");
            //            _room = value;
            //            NotifyPropertyChanged("Room");
            //        }
            //    }
            //}

            

          
            //private UserProfile _userProfile;

            //[Column]
            //public UserProfile UserProfile
            //{
            //    get
            //    {
            //        return _userProfile;
            //    }
            //    set
            //    {
            //        if (_userProfile != value)
            //        {
            //            NotifyPropertyChanging("UserProfile");
            //            _userProfile = value;
            //            NotifyPropertyChanged("UserProfile");
            //        }
            //    }
            //}


            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

           
            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            #endregion

            #region INotifyPropertyChanging Members

            public event PropertyChangingEventHandler PropertyChanging;

             
            private void NotifyPropertyChanging(string propertyName)
            {
                if (PropertyChanging != null)
                {
                    PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
                }
            }

            #endregion
        }      

    }

