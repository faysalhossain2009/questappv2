﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
    /*
    public class Question
    {
        [Key]
        public int ID { get; set; }
        public string QuestionBody { get; set; }        
        public Option CorrectOption { get; set; }
        public string ImageURL { get; set; }
        public int CorrectOptionID { get; set; }
        public List<Option> Options { get; set; }

    }
      */

    [Table]
        public class Question : INotifyPropertyChanged, INotifyPropertyChanging
        {
            // Define ID: private field, public property and database column.
            private int _id;

            [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
            public int QuestionID
            {
                get
                {
                    return _id;
                }
                set
                {
                    if (_id != value)
                    {
                        NotifyPropertyChanging("QuestionID");
                        _id = value;
                        NotifyPropertyChanged("QuestionID");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private string _questionBody;

            [Column]
            public string QuestionBody
            {
                get
                {
                    return _questionBody;
                }
                set
                {
                    if (_questionBody != value)
                    {
                        NotifyPropertyChanging("QuestionBody");
                        _questionBody = value;
                        NotifyPropertyChanged("QuestionBody");
                    }
                }
            }

            // Define _imageURL: private field, public property and database column.
            private string _imageURL;

            [Column]
            public string ImageURL
            {
                get
                {
                    return _imageURL;
                }
                set
                {
                    if (_imageURL != value)
                    {
                        NotifyPropertyChanging("ImageURL");
                        _imageURL = value;
                        NotifyPropertyChanged("ImageURL");
                    }
                }
            }



            [Column]
            internal int _CorrectOptionID;

            private EntityRef<Option> _Option;

            [Association(Storage = "_Option", ThisKey = "_CorrectOptionID", OtherKey = "OptionID", IsForeignKey = true)]
            public Option Option
            {
                get { return this._Option.Entity; }
                set
                {
                    this._Option.Entity = value;
                    if (value != null)
                    {
                        _CorrectOptionID = value.OptionID;
                    }
                }
            }

            // Define CorrectOption: private field, public property and database column.
            //private Option _correctOption;

            //[Column]
            //public Option CorrectOption
            //{
            //    get
            //    {
            //        return _correctOption;
            //    }
            //    set
            //    {
            //        if (_correctOption != value)
            //        {
            //            NotifyPropertyChanging("CorrectOption");
            //            _correctOption = value;
            //            NotifyPropertyChanged("CorrectOption");
            //        }
            //    }
            //}

            /*
            [Column]
            internal int _RoomID;

            private EntityRef<Room> _Room;

            [Association(Storage = "_Room", ThisKey = "_RoomID", OtherKey = "RoomID", IsForeignKey = true)]
            public Room Room
            {
                get { return this._Room.Entity; }
                set
                {
                    this._Room.Entity = value;
                    if (value != null)
                    {
                        _RoomID = value.RoomID;
                    }
                }
            }
       
            */
            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            // Used to notify the page that a data context property changed
            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            #endregion

            #region INotifyPropertyChanging Members

            public event PropertyChangingEventHandler PropertyChanging;

            // Used to notify the data context that a data context property is about to change
            private void NotifyPropertyChanging(string propertyName)
            {
                if (PropertyChanging != null)
                {
                    PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
                }
            }

            #endregion
        }       
    
}
