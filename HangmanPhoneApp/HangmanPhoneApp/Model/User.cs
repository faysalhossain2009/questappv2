﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDatabaseWPApp.Model
{

        [Table(Name = "User")]
        public class User : INotifyPropertyChanged, INotifyPropertyChanging
        {

            private int _id;

            [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
            public int Id
            {
                get
                {
                    return _id;
                }
                set
                {
                    if (_id != value)
                    {
                        NotifyPropertyChanging("Id");
                        _id = value;
                        NotifyPropertyChanged("Id");
                    }
                }
            }

            private string _name;

            [Column(DbType = "nvarchar(255)", CanBeNull = false)]
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (_name != value)
                    {
                        NotifyPropertyChanging("Name");
                        _name = value;
                        NotifyPropertyChanged("Name");
                    }
                }
            }

            private string _password;

            [Column(DbType = "nvarchar(255)", CanBeNull = false)]
            public string Password
            {
                get
                {
                    return _password;
                }
                set
                {
                    if (_password != value)
                    {
                        NotifyPropertyChanging("Password");
                        _password = value;
                        NotifyPropertyChanged("Password");
                    }
                }
            }



            public event PropertyChangingEventHandler PropertyChanging;

            // Used to notify the data context that a data context property is about to change
            private void NotifyPropertyChanging(string propertyName)
            {
                if (PropertyChanging != null)
                {
                    PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            // Used to notify the page that a data context property changed
            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        
    }
}

