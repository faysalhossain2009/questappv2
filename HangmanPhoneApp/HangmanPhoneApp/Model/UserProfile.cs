﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest.Model
{
    [Table]
    public class UserProfile : INotifyPropertyChanged, INotifyPropertyChanging
    {
        /*
        public int UserId { get; set; }
        public string UserName { get; set; }
        */

         // Define ID: private field, public property and database column.
            private int _id;

            [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
            public int UserProfileID
            {
                get
                {
                    return _id;
                }
                set
                {
                    if (_id != value)
                    {
                        NotifyPropertyChanging("UserProfileID");
                        _id = value;
                        NotifyPropertyChanged("UserProfileID");
                    }
                }
            }

            // Define _questionBody: private field, public property and database column.
            private string _userName;

            [Column]
            public string UserName
            {
                get
                {
                    return _userName;
                }
                set
                {
                    if (_userName != value)
                    {
                        NotifyPropertyChanging("UserName");
                        _userName = value;
                        NotifyPropertyChanged("UserName");
                    }
                }
            }
    
         #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            // Used to notify the page that a data context property changed
            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            #endregion

         #region INotifyPropertyChanging Members

            public event PropertyChangingEventHandler PropertyChanging;

            // Used to notify the data context that a data context property is about to change
            private void NotifyPropertyChanging(string propertyName)
            {
                if (PropertyChanging != null)
                {
                    PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
                }
            }

            #endregion
        }  
}

