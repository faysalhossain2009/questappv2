﻿using com.shephertz.app42.gaming.multiplayer.client.listener;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanPhoneApp
{
    class MyRoomRequestListener : RoomRequestListener
    {
        public void onGetLiveRoomInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onJoinRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
        {
            Debug.WriteLine("Joined Room");
        }

        public void onLeaveRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onLockPropertiesDone(byte result)
        {
            throw new NotImplementedException();
        }

        public void onSetCustomRoomDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
        {
            Debug.WriteLine("Subscribed Room");
        }

        public void onUnSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onUnlockPropertiesDone(byte result)
        {
            throw new NotImplementedException();
        }

        public void onUpdatePropertyDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent lifeLiveRoomInfoEvent)
        {
            throw new NotImplementedException();
        }
    }
}
