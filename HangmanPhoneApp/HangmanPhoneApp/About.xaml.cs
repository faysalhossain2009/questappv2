﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace HangmanPhoneApp
{
    public partial class About : PhoneApplicationPage
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Phone.Tasks.EmailComposeTask t = new Microsoft.Phone.Tasks.EmailComposeTask();
            t.Subject = "Feedback on Hangman Windows Phone application " + this.VersionTextBlock.Text.Substring("version ".Length);
            t.To = "vpstopwatch@hotmail.com";
            t.Show();
        }
    }
}
