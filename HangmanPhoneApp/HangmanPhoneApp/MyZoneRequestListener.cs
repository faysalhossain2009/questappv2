﻿using com.shephertz.app42.gaming.multiplayer.client.listener;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using HangmanPhoneApp;
using HangmanPhoneApp.ViewModels;
using com.shephertz.app42.gaming.multiplayer.client.command;
using Microsoft.Phone.Controls;

namespace quest
{
    public class MyZoneRequestListener:ZoneRequestListener
    {
        public MyZoneRequestListener() { 
        }


        public void onCreateRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
        {
            //MessageBox.Show(eventObj.getResult().ToString());
             Debug.WriteLine(eventObj.getResult().ToString());
            
            //Deployment.Current.Dispatcher.BeginInvoke(delegate
            //{
            //    System.Uri uri = (new Uri("/Layout/RoomDetails.xaml?id=" + eventObj.getData().getId() +
            //        ",name="+eventObj.getData().getName()+
            //         ",maxUser="+eventObj.getData().getMaxUsers()+
            //          ",owner="+eventObj.getData().getRoomOwner(), UriKind.RelativeOrAbsolute));
            //    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri);

            //    //NavigationService.Navigate(new Uri("/ResponsesPage.xaml", UriKind.Relative));
            //});
            
        }

        public void onDeleteRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onGetAllRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent eventObj)
        {
            //Debug.WriteLine(eventObj.getResult().ToString());

            if (eventObj.getResult() == WarpResponseResultCode.AUTH_ERROR)
            {
                //response = callback + " called" + Environment.NewLine + "WarpResponseResultCode : AUTH_ERROR";
            }
            else if (eventObj.getResult() == WarpResponseResultCode.BAD_REQUEST)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.CONNECTION_ERR)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.CONNECTION_ERROR_RECOVERABLE)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.RESOURCE_MOVED)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.RESOURCE_NOT_FOUND)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.RESULT_SIZE_ERROR)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
            {

                String[] roomIDs = eventObj.getRoomIds();
                //App.roomIds =new string[eventObj.getRoomIds().Length];

                App.roomIds = new string[roomIDs.Length];
                if(roomIDs!=null)
				{
					if (roomIDs.Length == 0) Debug.WriteLine("empty roomlist");
					int i = 0;
					foreach(string s in roomIDs)
					{
						//App.ViewModel.Items.Add(new ItemViewModel() 
						//{
						//    ID= i.ToString(),
						//    RoomName=eventObj.getResult().ToString()
						//});
						//Debug.WriteLine(s);
                        App.roomIds[i] = s;
						i++;
					}
				}

                Deployment.Current.Dispatcher.BeginInvoke(delegate
                {
                    System.Uri uri = (new Uri("/Layout/JoinContest.xaml", UriKind.RelativeOrAbsolute));
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri);

                    //NavigationService.Navigate(new Uri("/ResponsesPage.xaml", UriKind.Relative));
                });

            }
            else if (eventObj.getResult() == WarpResponseResultCode.SUCCESS_RECOVERED)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.UNKNOWN_ERROR)
            {
            }
            else if (eventObj.getResult() == WarpResponseResultCode.USER_PAUSED_ERROR)
            {
            }
            else
            {

            }

            
        }

        public void onGetLiveUserInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onGetMatchedRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent matchedRoomsEvent)
        {
            throw new NotImplementedException();
        }

        public void onGetOnlineUsersDone(com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent eventObj)
        {
            throw new NotImplementedException();
        }

        public void onSetCustomUserDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
        {
            throw new NotImplementedException();
        }
    }
}
