﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using com.shephertz.app42.gaming.multiplayer.client;
using com.shephertz.app42.paas.sdk.windows;
using com.shephertz.app42.paas.sdk.windows.user;
using quest;
using System.Diagnostics;


namespace HangmanPhoneApp.Layout
{
    public partial class SignUp : PhoneApplicationPage
    {
        public SignUp()
        {
            InitializeComponent();
        }

        private void register_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            String userName = username.Text;   
            String pwd  = password.Password;
            String emailId = email.Text;
            Debug.WriteLine(userName+" "+pwd );
            App42API.Initialize(GlobalContext.API_KEY,GlobalContext.SECRET_KEY);
            UserService userService = App42API.BuildUserService();  
            userService.CreateUser(userName,pwd,emailId, new Callback()); 
        }
            public class Callback : App42Callback  
            {  	
	            public void OnSuccess(Object response)  
	            {  
		            User user = (User) response;
		            Console.WriteLine("userName is " + user.GetUserName());
		            Console.WriteLine("emailId is " + user.GetEmail());

                    Debug.WriteLine(user.GetUserName() + " " + user.GetEmail());
		            String jsonResponse = user.ToString();
                  //  NavigationService.Navigate(new Uri("/Layout/Login.xaml", UriKind.Relative));
	            }  
	            public void OnException(App42Exception exception)  
	            {  
		            Console.WriteLine("Exception Message : " + exception);  
	            }  
            }  

		
        }
    }
