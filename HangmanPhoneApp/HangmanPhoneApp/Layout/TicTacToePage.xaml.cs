﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace HangmanPhoneApp.Layout
{
    public partial class TicTacToePage : PhoneApplicationPage
    {

        // Boolean variables to track whose move it is and if the game is over
        Boolean isX, GameOver;
        // An array to track the contents of each box (0 = empty 1 = X 2 = O)
        int[] boxValue;
        // Helpful images
        Image myXImage, myOimage, myEmptyImage;
        // Counting moves
        int MoveCount;

        public TicTacToePage()
        {
            InitializeComponent();
            boxValue = new int[10];
            //STore the images for use in the game
            myXImage = new Image();
            //myXImage.Source = new BitmapImage(new Uri("/Assets/Tic Tac Toe Tiles/cross.png", UriKind.Relative));
            myXImage.Source = new BitmapImage(new Uri("/Assets/Tic Tac Toe Tiles/swords.png", UriKind.Relative));
            myOimage = new Image();
            //myOimage.Source = new BitmapImage(new Uri("/Assets/Tic Tac Toe Tiles/circle.png", UriKind.Relative));
            myOimage.Source = new BitmapImage(new Uri("/Assets/Tic Tac Toe Tiles/shield.png", UriKind.Relative));
            myEmptyImage = new Image();
            myEmptyImage.Source = new BitmapImage(new Uri("/Assets/Tic Tac Toe Tiles/empty.png", UriKind.Relative));
            //Set the initial state of the game board
            
            resetBoxes();
        }
        
        void resetBoxes()
        {
            myStoryboard.Stop();
            playAgain.Visibility = Visibility.Collapsed;
            GameOver = false;
            MoveCount = 0;
            
            isX = true;
            // Set all boxes to empty
            for (int i = 0; i < 10; i++)
                boxValue[i] = 0;
            // Display empty images in all boxes on screen
            image1.Source = myEmptyImage.Source;
            image1.Opacity = 0;
            image2.Source = myEmptyImage.Source;
            image2.Opacity = 0;
            image3.Source = myEmptyImage.Source;
            image3.Opacity = 0;
            image4.Source = myEmptyImage.Source;
            image4.Opacity = 0;
            image5.Source = myEmptyImage.Source;
            image5.Opacity = 0;
            image6.Source = myEmptyImage.Source;
            image6.Opacity = 0;
            image7.Source = myEmptyImage.Source;
            image7.Opacity = 0;
            image8.Source = myEmptyImage.Source;
            image8.Opacity = 0;
            image9.Source = myEmptyImage.Source;
            image9.Opacity = 0;
            GameOver = false;
            MoveCount = 0;
            nextMove.Text = "Next Move ";
            image10.Source = myXImage.Source;

        }

        private void image1_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[1] == 0 && !GameOver)
            {
                boxValue[1] = updateBox();
                if (boxValue[1] == 1)
                {
                    image1.Source = myXImage.Source;
                    image1.Opacity = 100;
                }
                else
                {
                    image1.Source = myOimage.Source;
                    image1.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[2] == 0 && !GameOver)
            {
                boxValue[2] = updateBox();
                if (boxValue[2] == 1)
                {
                    image2.Source = myXImage.Source;
                    image2.Opacity = 100;
                }
                else
                {
                    image2.Source = myOimage.Source;
                    image2.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image3_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[3] == 0 && !GameOver)
            {
                boxValue[3] = updateBox();
                if (boxValue[3] == 1)
                {
                    image3.Source = myXImage.Source;
                    image3.Opacity = 100;
                }
                else
                {
                    image3.Source = myOimage.Source;
                    image3.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image4_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[4] == 0 && !GameOver)
            {
                boxValue[4] = updateBox();
                if (boxValue[4] == 1)
                {
                    image4.Source = myXImage.Source;
                    image4.Opacity = 100;
                }
                else
                {
                    image4.Source = myOimage.Source;
                    image4.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image5_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[5] == 0 && !GameOver)
            {
                boxValue[5] = updateBox();
                if (boxValue[5] == 1)
                {
                    image5.Source = myXImage.Source;
                    image5.Opacity = 100;
                }
                else
                {
                    image5.Source = myOimage.Source;
                    image5.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image6_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[6] == 0 && !GameOver)
            {
                boxValue[6] = updateBox();
                if (boxValue[6] == 1)
                {
                    image6.Source = myXImage.Source;
                    image6.Opacity = 100;
                }
                else
                {
                    image6.Source = myOimage.Source;
                    image6.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image7_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[7] == 0 && !GameOver)
            {
                boxValue[7] = updateBox();
                if (boxValue[7] == 1)
                {
                    image7.Source = myXImage.Source;
                    image7.Opacity = 100;
                }
                else
                {
                    image7.Source = myOimage.Source;
                    image7.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image8_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (boxValue[8] == 0 && !GameOver)
            {
                boxValue[8] = updateBox();
                if (boxValue[8] == 1)
                {
                    image8.Source = myXImage.Source;
                    image8.Opacity = 100;
                }
                else
                {
                    image8.Source = myOimage.Source;
                    image8.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        private void image9_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.

            if (boxValue[9] == 0 && !GameOver)
            {
                boxValue[9] = updateBox();
                if (boxValue[9] == 1)
                {
                    image9.Source = myXImage.Source;
                    image9.Opacity = 100;
                }
                else
                {
                    image9.Source = myOimage.Source;
                    image9.Opacity = 100;
                }
                if (IsWinner())
                    endGame();
            }
        }

        public int updateBox()
        {
            if (GameOver)
                return 0;
            MoveCount = MoveCount + 1;

            if (isX)
            {
                isX = !isX;
                image10.Source = myOimage.Source;
                image10.Opacity = 100;
                return 1;
            }
            else
            {
                isX = !isX;
                image10.Source = myXImage.Source;
                image10.Opacity = 100;
                return 2;
            }

        }

        private bool IsWinner()
        {
            bool HaveWinner = false;
            HaveWinner = false;
            if (boxValue[1] != 0 & boxValue[1] == boxValue[2] & boxValue[1] == boxValue[3])
                HaveWinner = true;
            if (boxValue[4] != 0 & boxValue[4] == boxValue[5] & boxValue[4] == boxValue[6])
                HaveWinner = true;
            if (boxValue[7] != 0 & boxValue[8] == boxValue[7] & boxValue[7] == boxValue[9])
                HaveWinner = true;
            if (boxValue[1] != 0 & boxValue[1] == boxValue[4] & boxValue[4] == boxValue[7])
                HaveWinner = true;
            if (boxValue[2] != 0 & boxValue[2] == boxValue[5] & boxValue[5] == boxValue[8])
                HaveWinner = true;
            if (boxValue[3] != 0 & boxValue[6] == boxValue[3] & boxValue[6] == boxValue[9])
                HaveWinner = true;
            if (boxValue[1] != 0 & boxValue[1] == boxValue[5] & boxValue[5] == boxValue[9])
                HaveWinner = true;
            if (boxValue[3] != 0 & boxValue[3] == boxValue[5] & boxValue[5] == boxValue[7])
                HaveWinner = true;

            if (HaveWinner == true)
                GameOver = true;
            // if there is no winner but we have taken 9 moves declare a draw (tie)
            if (MoveCount >= 9)
                TieGame();
            if (GameOver || MoveCount >= 9)
            {
                myStoryboard.Begin();
                playAgain.Visibility = Visibility.Visible;
            }
            return HaveWinner;
        }

        public void endGame()
        {
            nextMove.Text = "The Winner is";
            if (isX)
            {
                image10.Source = myOimage.Source;
                image10.Opacity = 100;
            }
            else
            {
                image10.Source = myXImage.Source;
                image10.Opacity = 100;
            }
        }


        // Display tie game information
        public void TieGame()
        {
            image10.Source = myEmptyImage.Source;
            image10.Opacity = 000;
            nextMove.Text = "Tie Game";

        }

        private void playAgain_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            resetBoxes();
            //NavigationService.Navigate(new Uri("/Layout/TicTacToePage.xaml", UriKind.Relative));

        }

        
        
    }
}