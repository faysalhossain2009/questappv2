﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using com.shephertz.app42.paas.sdk.windows.user;
using com.shephertz.app42.paas.sdk.windows;
using quest;

namespace HangmanPhoneApp.Layout
{
    public partial class login : PhoneApplicationPage
    {
        public login()
        {
            InitializeComponent();
        }

        private void Sign_In_Click(object sender, RoutedEventArgs e)
        {
            String userName = username.Text;   
            String pwd  = password.Password;
            App42API.Initialize(GlobalContext.API_KEY,GlobalContext.SECRET_KEY);
            UserService userService = App42API.BuildUserService();  
            userService.Authenticate(userName,pwd, new Callback()); 
        }
            public class Callback : App42Callback  
            {  
	            public void OnSuccess(Object response)  
	            {  
		            User user = (User) response;     
		            Console.WriteLine("userName is : " + user.GetUserName());
		            Console.WriteLine("sessionId is : " + user.GetSessionId());

                    GlobalContext.saveDataToStorage(GlobalContext.loggedUser, user.GetUserName());



                } 
	            public void OnException(App42Exception exception)  
	            {  
		            Console.WriteLine("Exception Message : " + exception);  
	            }  	
            }

            private void Button_Click(object sender, RoutedEventArgs e)
            {
                NavigationService.Navigate(new Uri("/Layout/SignUp.xaml", UriKind.Relative));
            }  
                
        }
    }



