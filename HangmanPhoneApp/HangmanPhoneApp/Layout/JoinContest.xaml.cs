﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HangmanPhoneApp;
using HangmanPhoneApp.ViewModels;

namespace quest
{
    public partial class JoinContest : PhoneApplicationPage
    {
        public JoinContest()
        {
            InitializeComponent();

            DataContext = App.ViewModel;
        }
        /*
        private static void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator.
                IsIndeterminate = isVisible;

            SystemTray.ProgressIndicator.
                IsVisible = isVisible;

        }
         * */
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                //SetProgressIndicator(true); 
                App.ViewModel.LoadData();
            }
        }

        private void MainLongListSelector_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
			
			// If selected item is null (no selection) do nothing
            if (MainLongListSelector.SelectedItem == null)
                return;

            GlobalContext.warpClient.JoinRoom((MainLongListSelector.SelectedItem as ItemViewModel).ID);
            GlobalContext.warpClient.SubscribeRoom((MainLongListSelector.SelectedItem as ItemViewModel).ID);
            
            // Navigate to the new page
            NavigationService.Navigate(new Uri("/Layout/GameSelect.xaml?selectedItem=" + (MainLongListSelector.SelectedItem as ItemViewModel).ID, UriKind.Relative));

            // Reset selected item to null (no selection)
            MainLongListSelector.SelectedItem = null;
        }
    }
}