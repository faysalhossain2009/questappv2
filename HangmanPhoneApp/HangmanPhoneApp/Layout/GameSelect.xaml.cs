﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace HangmanPhoneApp.Layout
{
    public partial class GameSelect : PhoneApplicationPage
    {
        public GameSelect()
        {
            InitializeComponent();
        }
      

        private void hangman_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 

            NavigationService.Navigate(new Uri("/GamePage.xaml", UriKind.Relative));
        }

        private void tic_tac_toe_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 

            NavigationService.Navigate(new Uri("/Layout/TicTacToePage.xaml", UriKind.Relative));
        }

        private void battleship_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 
			 MessageBoxResult mbr = MessageBox.Show("This feature will be implemented in the full version.", "Beta Version", MessageBoxButton.OKCancel);
        }

        private void toh_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 
			 MessageBoxResult mbr = MessageBox.Show("This feature will be implemented in the full version.", "Beta Version", MessageBoxButton.OKCancel);
        }

      
    }
}