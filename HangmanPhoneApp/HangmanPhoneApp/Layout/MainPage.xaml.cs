﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using HangmanPhoneApp.Layout;
using System.Threading;




namespace quest
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        BackgroundWorker backroungWorker;
        BackgroundWorker runThread;
        Popup myPopup;

        public MainPage()
        {
            myPopup = new Popup() { IsOpen = true, Child = new SplashScreen() };
            backroungWorker = new BackgroundWorker();
            RunBackgroundWorker();
            InitializeComponent();
           // mainPageStoryboard.Begin();
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

        }
      

      
            

        private void RunBackgroundWorker()
        {
            backroungWorker.DoWork += ((s, args) =>
            {
                Thread.Sleep(5000);

            });

            backroungWorker.RunWorkerCompleted += ((s, args) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    this.myPopup.IsOpen = false;
                }
            );
            });
            backroungWorker.RunWorkerAsync();
        }

        private void RunThreadWorker()
        {
            runThread.DoWork += ((s, args) =>
            {
                pBar.Visibility = Visibility.Visible;
                GlobalContext.warpClient.GetAllRooms();
            });

            runThread.RunWorkerCompleted += ((s, args) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    pBar.Visibility = Visibility.Collapsed;
                    NavigationService.Navigate(new Uri("/Layout/JoinContest.xaml", UriKind.Relative));
                }
            );
            });
            runThread.RunWorkerAsync();
        }

        private void singlePlayer_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 
			NavigationService.Navigate(new Uri("/Layout/GameSelect.xaml", UriKind.Relative));
        }

        private void hostContest_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300));
           // pBar.Visibility = Visibility.Visible;
			NavigationService.Navigate(new Uri("/Layout/HostContest.xaml", UriKind.Relative));
        }

        private void joinContest_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300));
           // pBar.Visibility = Visibility.Visible;
            GlobalContext.warpClient.GetAllRooms();
            //NavigationService.Navigate(new Uri("/Layout/JoinContest.xaml", UriKind.Relative));
          //  RunThreadWorker();
        }

        private void requestContest_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 

            MessageBoxResult mbr = MessageBox.Show("This feature will be implemented in the full version. It will be available as In-app purchase.", "Beta Version", MessageBoxButton.OKCancel);

        }



        private void options_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 
        }

        private void editPlayer_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 
        }

     

        private void rateUs_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            Microsoft.Devices.VibrateController vibrate = Microsoft.Devices.VibrateController.Default;
            vibrate.Start(TimeSpan.FromMilliseconds(300)); 

            MessageBoxResult mbr = MessageBox.Show("This feature will be implemented when it will be uploaded to the store.", "Beta Version", MessageBoxButton.OKCancel);
        }
        
         protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            pBar.Visibility = Visibility.Collapsed;
         }
        //private void single_player_Click(object sender, RoutedEventArgs e)
        //{
        //    NavigationService.Navigate(new Uri("/Layout/GameSelect.xaml", UriKind.Relative));
        //}

        //private void achievement_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: Add event handler implementation here.
        //}

        //private void host_contest_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: Add event handler implementation here.
        //    NavigationService.Navigate(new Uri("/Layout/HostContest.xaml", UriKind.Relative));
        //}

        //private void join_contest_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: Add event handler implementation here.
        //    GlobalContext.warpClient.GetAllRooms();

        //    //NavigationService.Navigate(new Uri("/Layout/JoinContest.xaml", UriKind.Relative));
        //}


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}