﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace HangmanPhoneApp.Layout
{
    public partial class RoomDetails : PhoneApplicationPage
    {
        public RoomDetails()
        {
            InitializeComponent();
            // GetLiveRoomInfo("12");
        }
           protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string id = "", nam = "", ownr = "", userNo = "";
            int index=0, mx=0;
            string roomName="", own="";
            if (NavigationContext.QueryString.TryGetValue("id", out id))
            {
                index = int.Parse(id);
                // DataContext = App.ViewModel.Items[index];
            }
            if (NavigationContext.QueryString.TryGetValue("name", out nam))
            {
                roomName
                    = nam;
                // DataContext = App.ViewModel.Items[index];
            }
            if (NavigationContext.QueryString.TryGetValue("maxUser", out userNo))
            {
                mx = int.Parse(userNo);
                // DataContext = App.ViewModel.Items[index];
            }
            if (NavigationContext.QueryString.TryGetValue("owner", out ownr))
            {
                own = ownr;
                // DataContext = App.ViewModel.Items[index];
            }
            roomID.Text = index + "";
            maxUser.Text = mx + "";
            name.Text = roomName;
            owner.Text=own;
        }
       
    
    }
}