﻿using com.shephertz.app42.gaming.multiplayer.client;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quest
{
    public class GlobalContext
    {
        public static String API_KEY = "0f5cdd7549c2a64336c3ef4388064583a63aaf64fe664a42b55b63649c106171";
        public static String SECRET_KEY = "54acd325aabdf7771dc21cb8c57e7e3417a5dbfed2d39458109eddf64a9404b0";
        public static String loggedUser = null ;

        // Game room id used in this tutorial. 
        // NOTE* replace with your room's id that you created from 
        // App HQ dashboard (http://apphq.shephertz.com).
        
        public static WarpClient warpClient;
        public static MyZoneRequestListener myZoneRequestListener;
        //public static ConnectionListener conListenObj;
        //public static RoomReqListener roomReqListenerObj;
        //public static NotificationListener notificationListenerObj;

        public static void saveDataToStorage(string key,string value)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            // txtInput is a TextBox defined in XAML.
            if (!settings.Contains(key))
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key] = value;
            }
            settings.Save();
        }

        public static string readDataFromStorage(string key)
        {

            try
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
                {
                    return IsolatedStorageSettings.ApplicationSettings[key] as string;
                }
            }
            catch (Exception exc)
            {
                return null;
            }
            

            return null;
        }
    }
}
